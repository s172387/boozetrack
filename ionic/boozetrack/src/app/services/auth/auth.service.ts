import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

const TOKEN_KEY = 'X-Auth-Token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState$: BehaviorSubject<boolean> = new BehaviorSubject(null);
  authState: firebase.User = null;

  constructor(
    private storage: Storage,
    private platform: Platform,
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private router: Router
  ) {

    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth;
    });

    this.platform.ready().then( _ => {
      this.checkToken();
    });
  }

  private checkToken() {
    this.storage.get(TOKEN_KEY).then( res => {
      if (res) {
        this.authState$.next(true);
      }
    });
  }

  public async login(): Promise<void> {
    await this.anonymousLogin();
    console.log('User uid: ', this.currentUserId);

    this.storage.set(TOKEN_KEY, this.currentUserId).then( res => {
      this.authState$.next(true);
    });
  }

  public async logout(): Promise<void> {
    await this.afAuth.auth.signOut();
    this.storage.remove(TOKEN_KEY).then( _ => {
      this.authState$.next(false);
    });
    this.router.navigate(['/']);
  }

  public getAuthStateObserver(): Observable<boolean> {
      return this.authState$.asObservable();
    }

  public isAuthenticated() {
    return this.authState$.value;
  }

  // Returns true if user is logged in
  get authenticated(): boolean {
    return this.authState !== null;
  }

  // Returns current user data
  get currentUser(): firebase.User {
    return this.authenticated ? this.authState : null;
  }

  // Returns current user UID
  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  // Anonymous User
  get currentUserAnonymous(): boolean {
    return this.authenticated ? this.authState.isAnonymous : false
  }

  // Returns current user display name or Guest
  get currentUserDisplayName(): string {
    if (!this.authState) { return 'Guest' }
    else if (this.currentUserAnonymous) { return 'Anonymous' }
    else { return this.authState['displayName'] || 'User without a Name' }
  }

  //// Anonymous Auth ////
  public async anonymousLogin(): Promise<void> {
    return this.afAuth.auth.signInAnonymously()
    .then((user) => {
      this.authState = user.user;
      this.updateUserData();
    })
    .catch(error => console.log(error));
  }

  //// Helpers ////

  private updateUserData(): void {
    // Writes user name and email to realtime db
    // useful if your app displays information about users or for admin features

    let path = `users/${this.currentUserId}`; // Endpoint on firebase
    let data = {
                  email: this.authState.email,
                  name: this.authState.displayName,
                  last_login: new Date().toLocaleString()
                }

    this.db.object(path).update(data)
    .catch(error => console.log(error));

  }
}
