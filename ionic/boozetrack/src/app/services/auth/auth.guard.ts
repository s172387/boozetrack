import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    console.log('Auth guard, state: ', state);
    console.log('Auth guard, next: ', next);

    const isAuthenticated = this.authService.isAuthenticated();

      if (!isAuthenticated) {
        this.router.navigate(['/'], {
          queryParams: {
            return: state.url
          }
        });
      }

    return isAuthenticated;
  }
}