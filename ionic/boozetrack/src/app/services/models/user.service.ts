import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { ToastController } from '@ionic/angular';
import { Drink } from 'src/app/models/drink';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  toast: HTMLIonToastElement;

  constructor(
    private authService: AuthService,
    private db: AngularFireDatabase,
    public toastController: ToastController
  ) { }

  public async isAuthenticated(): Promise<void> {
    if (!this.authService.authenticated) {
      await this.authService.login();
    }
  }

  public async getDrinksObservable(): Promise<any> {
    await this.isAuthenticated();
    const path = `users/${this.authService.currentUserId}/drinks`; // Endpoint on firebase
    return this.db.list(path).valueChanges();
  }

  public async getDrinksSnapshotObservable(): Promise<any> {
    await this.isAuthenticated();
    const path = `users/${this.authService.currentUserId}/drinks`; // Endpoint on firebase
    return this.db.list(path).snapshotChanges();
  }

  public async addDrink(drink: Drink): Promise<void> {
    const path = `users/${this.authService.currentUserId}/drinks`; // Endpoint on firebase
    const uniqueId = this.db.createPushId();
    let data = {};
    data[uniqueId] = drink;

    return this.db.object(path).update(data)
    .catch(error => console.log(error));
  }

  public async presentToast(msg: string): Promise<void> {
    if (this.toast) {
      try {
        this.toast.dismiss();
      } catch (e) {}
    }

    this.toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
      color: 'success',
    });
    this.toast.present();
  }

  public async removeDrink(drink: Drink): Promise<void> {
    let path = `users/${this.authService.currentUserId}/drinks`; // Endpoint on firebase
    path = path + `/${drink.drinkGuid}`;
    this.db.object(path).remove();
  }

}
