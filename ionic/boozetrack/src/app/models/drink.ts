export class Drink {
    description: string;
    calories: number;
    type: string;
    /* unique ID from firebase */
    drinkGuid: string;

    constructor(d: string, c: number, t: string) {
        this.description = d;
        this.calories = c;
        this.type = t;
    }

    static fixedList(): Drink[] {
        let drinks: Drink[] = [
            new Drink('San Miguel', 150, 'beer'),
            new Drink('Tubørg', 130, 'beer'),
            new Drink('Corona', 95, 'beer'),
            new Drink('Monkey Shoulder', 60, 'flask'),
            new Drink('Sangre de Toro', 75, 'wine'),
            new Drink('Mojito', 110, 'umbrella'),
            new Drink('jägermeister', 200, 'pint'),
        ];

        return drinks;
    }

}
