import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserService } from 'src/app/services/models/user.service';
import { Drink } from 'src/app/models/drink';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  hideTour: boolean;
  userId: string;
  drinkList: any[];
  loading: boolean;

  constructor(
    private storage: Storage,
    private authServ: AuthService,
    private userServ: UserService
  ) {
    this.hideTour = true;
    this.drinkList = [];
    this.init();
  }

  public setHideTour(): void {
    this.hideTour = true;
    this.storage.set(this.userId + '_hideTour', 'true');
  }

  public async getList(): Promise<void>{
    this.loading = true;
    const obs = await this.userServ.getDrinksSnapshotObservable();
    obs.subscribe(res => {
      this.drinkList = [];
      res.forEach(action => {
        console.log(action.key);
        console.log(action.payload.val());
        let drink: Drink = (action.payload.val()) as Drink;
        drink.drinkGuid = action.key;

        this.drinkList.push(drink);
      });
      this.loading = false;
    });
  }

  public async init(): Promise<void> {
    if (!this.authServ.authenticated) {
      await this.authServ.login();
    }
    this.userId = this.authServ.currentUserId;
    this.storage.get(this.userId + '_hideTour').then((val) => {
      this.hideTour = !!val;
    });
    await this.getList();
  }

  public async addDrink(): Promise<void> {
    const data: Drink = new Drink('san miguel', 150, 'beer');

    await this.userServ.addDrink(data);
  }

  public async removeDrink(drink: any) {
    if (drink) {
      await this.userServ.removeDrink(drink);
    }
  }

}
