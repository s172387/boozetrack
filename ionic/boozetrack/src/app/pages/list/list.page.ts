import { Component, OnInit } from '@angular/core';
import { Drink } from 'src/app/models/drink';
import { UserService } from 'src/app/services/models/user.service';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  public drinks: Array<Drink> = [];

  constructor(private userService: UserService) {
    this.drinks = Drink.fixedList();
    console.log('fixed drinks: ', this.drinks);
  }

  ngOnInit() {
  }

  public async addDrink(drink: Drink): Promise<void> {
    await this.userService.addDrink(drink);
    await this.userService.presentToast('The drink ' + drink.description + ' has been added successfully');
  }

  public async search(event: any): Promise<void> {
    let text: string = event.target.value;
    console.log(text);
    if (text) {
      this.drinks = Drink.fixedList().filter(drink => drink.description.toLowerCase().indexOf(text.toLowerCase()) > -1);
    } else {
      this.drinks = Drink.fixedList();
    }
  }

  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
