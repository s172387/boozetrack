# Project Title

BoozeTrack
02808 Personal Data Interaction for Mobile and Wearables F19

## Getting Started

Clone the project

```
git clone https://gitlab.gbar.dtu.dk/s172387/boozetrack.git
```

Run the project

```
ionic serve
```

### Prerequisites

If you are on Windows, make sure to download and install [Git for Windows](http://git-scm.com/download/win)
First, we will go and install the most recent version of [Apache Cordova](http://cordova.apache.org/), which will take our app and bundle it into a native wrapper to turn it into a traditional native app.

To install Cordova, make sure you have [Node.js](http://nodejs.org/) installed, then run

```
$ sudo npm install -g cordova
```

To install [Ionic](https://ionicframework.com/), simply run:

```
$ sudo npm install -g ionic
```

To install [typescript](https://www.typescriptlang.org/), run:
```
$ sudo npm install -g typescript
```

## Authors

* **Mario Padilla** - *Initial work* - [MarioPadilla](https://github.com/MarioPadilla)

See also the list of [contributors](https://gitlab.gbar.dtu.dk/s172387/boozetrack/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
